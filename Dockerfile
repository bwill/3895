FROM ubuntu:18.04 as ubuntu
RUN apt-get update \
        && apt-get -y install --no-install-recommends jq \
        && rm -r /var/lib/dpkg/*

FROM scratch

COPY --from=ubuntu / /
